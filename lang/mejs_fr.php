<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;
 
$GLOBALS[$GLOBALS['idx_lang']] = array(

	// M
	'mejs_titre' => 'MediaElementPlayer',

	// C
	'cfg_exemple' => 'Exemple',
	'cfg_exemple_explication' => 'Explication de cet exemple',
	'cfg_titre_parametrages' => 'Paramétrages',
  
  // L
  'label_maxheight' => 'Hauteur maxi. par défaut (px)',
	'label_maxwidth' => 'Largeur maxi. par défaut (px)',

	// T
	'titre_page_configurer_mejs' => 'Lecteur MediaElement',
  'titre_configurer_mejs' => 'Configuration du Lecteur MediaElement'
);

?>