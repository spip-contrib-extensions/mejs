<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// M
	'mejs_description' => 'MediaElement.js un lecteur audio et vidéo qui enrichit sur les capacités natives de chaque navigateur.',
	'mejs_nom' => 'MediaElementPlayer',
	'mejs_slogan' => 'Lecteur audio et vidéo HTML5',
);

?>